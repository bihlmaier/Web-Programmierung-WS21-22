import datetime
from django.db import models
from django.utils import timezone


# Create your models here.
class List(models.Model):
    name = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date created')

    def __str__(self):
        return self.name


class Task(models.Model):
    list = models.ForeignKey(List, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    done = models.BooleanField(default=False)
    created = models.DateField('date created')
    due = models.DateTimeField('due date')

    def __str__(self):
        return self.name
