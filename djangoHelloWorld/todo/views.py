from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404


# Create your views here.
from django.urls import reverse

from todo.models import List, Task


def index(request):
    list = List.objects.all()
    context = {'list': list}
    return render(request, 'todo/index.html', context)

def list(request, list_id):
    list = get_object_or_404(List, pk=list_id)
    return render(request, 'todo/list.html', {'list': list})