from django.shortcuts import render
from django.http import HttpResponse

from . import models

# Create your views here.

def index(request):
    context = {
        "name": "Kai",
        "liste": ["Eins", "Zwei", "Drei", "Vier", "Fünf"],
    }
    return render(request, "hello/test1.html", context)
    
def test2(request):
    context = {
        "name": "Kai",
        "liste": [
            {"even": False, "value": "Eins" }, 
            {"even": True, "value": "Zwei" }, 
            {"even": False, "value": "Drei" }, 
            {"even": True, "value": "Vier" }, 
            {"even": False, "value": "Fünf" }, 
        ]
    }
    return render(request, "hello/test2.html", context)

def einkaufen(request):
    context = {}
    produkte = models.Produkt.objects.all()
    context["produkte"] = produkte
    return render(request, "hello/einkaufen.html", context)